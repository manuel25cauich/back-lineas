<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class pedidosDetalle extends Model
{
    protected $table = "pedidos_det";

    public function compania(){
        return $this->hasOne(Compania::class,"id","carrier");
        // return $this->hasMany(ArticuloXSucursal::class,"ID_ART","ART_ID");


    }
    public function producto(){
        return $this->hasOne(Productos::class,"sku","sku");
        // return $this->hasMany(ArticuloXSucursal::class,"ID_ART","ART_ID");


    }
}
