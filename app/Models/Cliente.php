<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = "clientes";
    public function vendedor(){
        return $this->hasOne(Vendedor::class,"cve_vendedor","cve_vendedor");
    }

}
