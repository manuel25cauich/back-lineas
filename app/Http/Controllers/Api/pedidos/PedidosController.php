<?php

namespace App\Http\Controllers\Api\pedidos;

use Exception;
use App\Http\Controllers\Controller;
use App\Models\Adquisicion;
use App\Models\Cliente;
use App\Models\Compania;
use App\Models\Linea;
use App\Models\LineaConsumo;
use App\Models\Pedidos;
use App\Models\pedidosDetalle;
use App\Models\Proyecto;
use App\Models\ProyectoUsuario;
use App\Models\Sitio;
use App\Models\Vendedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\isNull;
use function PHPUnit\Framework\returnSelf;

class PedidosController extends Controller
{
    public function getPedidos(Request $request)
    {
        try {
            // $lineas = Linea::get();


            $pedidos = Pedidos::with('tipo', 'cliente')->select('*')
            ->where('tipoPedido', 0)
            ->where('eliminado', '0');

            if($request->filtro == '1'){
                $pedidos->where(function($query){
                    $query->where('recibido_almacen','0')
                          ->orWhere('surtiendo_almacen','0')
                          ->orWhere('listo_almacen','0')
                          ->orWhere('entrega_vendedor','0');
                })->where('cancelado','0');
            }




            if($request->rol == 2){
                $vendedor = Vendedor::where('usuario_id',$request->id)->first();
                if($vendedor){
                    $pedidos->where('clave_vendedor',$vendedor->cve_vendedor);
                }
            }


            return response()->json([
                "data" => $pedidos->orderBy('numero_pedido', 'desc')->get()
            ]);
            //code...
        } catch (Exception $e) {
            //throw $th;
            return response()->json([
                "data" => [],
                "error" => $e->getMessage()
            ]);
        }
    }

    public function getClientes(Request $request)
    {
        $clientes = Cliente::get();

        return response()->json([
            "clientes" => $clientes
        ]);
    }

    public function guardarPedido(Request $request)
    {
        $clave = null;
        $segmento = '1001';
        if($request->rol == 2){
            $vendedor = Vendedor::where('usuario_id', $request->id)->first();
            if($vendedor){
                $clave = $vendedor->cve_vendedor;
            }
        }
        #aqui se calcula el siguiente numero 20240321
        $fecha = now();
        $mes = $fecha->month < 10 ? '0' . $fecha->month : $fecha->month;
        $dia = $fecha->day < 10 ? '0' . $fecha->day : $fecha->day;
        $fechaString = $fecha->year . '' . $mes . '' . $dia;

        $numeroP = Pedidos::select('*')->where("numero_pedido", 'like', $fechaString . '%')
            ->orderBy('fecha_pedido', 'desc')
            ->first();


        if (!is_null($numeroP)) {
            $n = substr($numeroP['numero_pedido'], -4);
            $n = intval($n) + 1;
            $segmento = $n;
        }

        // return response()->json([
        //     "numer"=>$segmento
        // ]);
        $totalProductos = 0;

        // foreach ($request->listaCompa as $key => $value) {
        //     $totalProductos += $value['cantidad'];
        // }

        try {
            DB::beginTransaction();
            $newPedido = new Pedidos();
            $newPedido->numero_pedido = $fechaString . '_' . $segmento;
            $newPedido->fecha_pedido = $fecha;
            $newPedido->tipo_pedido = $request->tipo == '1' ? 'E' : 'G';
            $newPedido->clave_cliente = $request->cliente->cve_cliente ?? null;
            $newPedido->tipo_pedido_id = intval($request->tipo);
            $newPedido->total_productos = $totalProductos;
            $newPedido->clave_vendedor = $clave;
            $newPedido->tipoPedido = 0;
            $newPedido->save();

            if ($newPedido) {

                foreach ($request->listaCompa as $key => $value) {
                    $newPedidoDet = new pedidosDetalle();
                    $newPedidoDet->numero_pedido = $fechaString . '_' . $segmento;
                    $newPedidoDet->carrier = $value['carrier'];
                    $newPedidoDet->cantidad = $value['cantidad'];
                    $newPedidoDet->sku = $value['sku'];
                    $newPedidoDet->save();
                }
            }
            DB::commit();

            return response()->json([
                "success" => true,
                "segmento"=>$fechaString . '_' . $segmento
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            //throw $th;
            return response()->json([
                "success" => false,
                "msg" => $e->getMessage()
            ]);
        }



        return response()->json([
            "date" => $fechaString,
            "numeroP" => $numeroP,
            "segmento" => $segmento
        ]);
    }

    public function eliminarPedido(Request $request)
    {
        try {
            DB::beginTransaction();
            $pedido = Pedidos::where('id', $request->id)->first();
            $pedido->eliminado = '1';
            $pedido->save();


            $detallesP = pedidosDetalle::where('numero_pedido', $pedido->numero_pedido)->get();

            foreach ($detallesP as $item) {
                $item->eliminado = '1';
                $item->save();
            }


            DB::commit();
            return response()->json([
                "success" => true
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                "success" => false,
                "msg" => $e->getMessage()
            ]);
        }
    }

    public function getPedido(Request $request)
    {
        $pedido = Pedidos::with('detalle.compania', 'cliente')->where('id', $request->id)->first();


        return response()->json([
            "pedido" => $pedido
        ]);
    }

    public function guardarDetalles(Request $request)
    {
        try {

            DB::beginTransaction();

            $detallesEliminar = pedidosDetalle::where('numero_pedido', $request->numero_pedido)->get();
            //se eliminan los detalles anteriores
            foreach ($detallesEliminar as $obj) {
                $obj->delete();
            }

            //creacion de los nuevos pedido
            $totalProductos = 0;
            foreach ($request->detalle as $key => $value) {

                // if ($value['cantidad'] > 0) {
                $totalProductos += $value['cantidad'];
                $newPedidoDet = new pedidosDetalle();
                $newPedidoDet->numero_pedido = $request->numero_pedido;
                $newPedidoDet->carrier = $value['carrier'];
                $newPedidoDet->cantidad = $value['cantidad'];
                $newPedidoDet->sku = $value['sku'];

                $newPedidoDet->save();
                // }
            }

            $pedido = Pedidos::where('numero_pedido', $request->numero_pedido)->first();
            $pedido->total_productos = $totalProductos;
            $pedido->tipo_pedido_id = intval($request->tipo);
            $pedido->tipo_pedido = $request->tipo == '1' ? 'E' : 'G';
            $pedido->clave_cliente = $request->cliente['cve_cliente'] ?? null;
            $pedido->save();

            DB::commit();
            return response()->json([
                "success" => true
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                "success" => false,
                "msg" => "Error al actualizar el pedido",
                "error" => $e->getMessage()
            ]);
        }
    }

    public function cancelarpedido(Request $request)
    {
        try {
            DB::beginTransaction();
            $pedido = Pedidos::where('id', $request->id)->first();
            $pedido->cancelado = '1';
            $pedido->referencia = $request->referencia;
            $pedido->save();

            DB::commit();
            return response()->json([
                "success"=>true
            ]);

        } catch (Exception $e) {
            DB::rollBack();

            return response()->json([
                "success"=>false,
                "msg"=>"Error al Cancelar",
                "error"=>$e->getMessage()
            ]);
        }
    }

    public function confirmarPedido(Request $request){
        $pedido = Pedidos::where('id', $request->id)->first();

        $pedido->recibido_almacen = '1';
        $pedido->fecha_recibido_almacen = now();
        $pedido->save();

        return response()->json([
            "success"=>true
        ]);

    }

    public function editarPedido(Request $request){
        $pedido = Pedidos::where('id', $request->id)->first();
        if($pedido){
            $pedido->recibido_almacen = '0';
            $pedido->fecha_recibido_almacen = null;
            $pedido->save();
        }

        return response()->json([
            "success"=>true
        ]);

    }
}
