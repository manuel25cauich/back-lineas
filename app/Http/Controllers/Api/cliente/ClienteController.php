<?php

namespace App\Http\Controllers\Api\cliente;

use Exception;
use App\Http\Controllers\Controller;
use App\Models\Adquisicion;
use App\Models\Cliente;
use App\Models\Compania;
use App\Models\Linea;
use App\Models\LineaConsumo;
use App\Models\Pedidos;
use App\Models\pedidosDetalle;
use App\Models\Proyecto;
use App\Models\ProyectoUsuario;
use App\Models\Roles;
use App\Models\Sitio;
use App\Models\Usuario;
use App\Models\Vendedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\isNull;
use function PHPUnit\Framework\returnSelf;

class ClienteController extends Controller
{
    public function getClientesVendedor(Request $request)
    {
        $clientes = Cliente::with('vendedor')->select('*');

        if ($request->rol != 1) {
            $vendedor = Vendedor::where('usuario_id', $request->id)->first();
            $clientes->where('cve_vendedor', $vendedor->cve_vendedor);
        }else{
            $clientes->where('cve_vendedor','!=',null);
        }

        return response()->json([
            "clientes" => $clientes->where('activo', '1')->orderBy('id', 'desc')->get()
        ]);
    }
    public function getVendedor(Request $request)
    {
        $vendedor = Vendedor::where('usuario_id', $request->id)->first();
        $cve_vendedor = null;
        if (!is_null($vendedor)) {
            $cve_vendedor = $vendedor->cve_vendedor;
        }

        return response()->json([
            "vendedor" => $cve_vendedor,
            "vendedores" => $vendedor
        ]);
    }

    public function getVendedoresC()
    {
        $vendedores = Vendedor::get();

        return response()->json([
            "vendedores" => $vendedores
        ]);
    }

    public function guardarCliente(Request $request)
    {
        try {
            $numeroC = Cliente::select('*')
                ->orderBy('id', 'desc')
                ->first();

            $segmento = '10000';
            if (!is_null($numeroC)) {
                $n = $numeroC['cve_cliente'];
                $n = intval($n) + 1;
                $segmento = $n;
            }

            DB::beginTransaction();
            $cliente = new Cliente();
            if (!is_null($request->id)) {
                $cliente = Cliente::where('id', $request->id)->first();
            }
            // $sitio->colonia_sitio = $request->colonia_sitio['d_asenta']??$request->colonia_sitio;
            if (is_null($request->id)) {
                $cliente->cve_cliente = strval($segmento);
            }
            $cliente->nombre_cliente = $request->nombre_cliente;
            $cliente->cve_vendedor = '001';
            $cliente->alias_cliente = $request->alias_cliente;
            $cliente->calleynumero = $request->calleynumero;
            $cliente->cp_cliente = $request->cp_cliente;
            $cliente->colonia = $request->colonia['d_asenta'] ?? $request->colonia;
            $cliente->municipio = $request->municipio['D_mnpio'] ?? $request->municipio;
            $cliente->estado = $request->estado['d_estado'] ?? $request->estado;
            $cliente->referencia = $request->referencia;
            $cliente->latitud = $request->latitud;
            $cliente->longitud = $request->longitud;
            $cliente->telefonoContacto = $request->telefonoContacto;
            $cliente->telefonoNotificacionesSms = $request->telefonoNotificacionesSms;
            $cliente->whatsapp = $request->whatsapp;
            $cliente->save();


            if ($cliente->usuario_id == null) {
                $usuario = new Usuario();
                $usuario->nombre = $request->nombre_cliente;
                $usuario->correo = $request->correo;
                $usuario->password = $request->pass;
                $usuario->rol = $request->rol['id'];

                if ($usuario->save()) {
                    $cliente->usuario_id = $usuario->id;
                    $cliente->save();
                }
            } else {
                $usuario = Usuario::where('id', $cliente->usuario_id)->first();
                $usuario->nombre = $request->nombre_cliente;
                $usuario->correo = $request->correo;
                $usuario->password = $request->pass;
                $usuario->rol = $request->rol['id'];


                if ($usuario->save()) {
                    $cliente->usuario_id = $usuario->id;
                    $cliente->save();
                }
            }

            DB::commit();

            return response()->json([
                "success" => true
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                "success" => false,
                "msg" => "error al registrar",
                "error" => $e->getMessage()
            ]);
        }
    }

    public function eliminarCliente(Request $request)
    {
        $cliente = Cliente::where('id', $request->id)->first();
        $cliente->activo = '0';
        $cliente->save();

        return response()->json([
            "success" => true
        ]);
    }

    public function getRolesClientes()
    {

        $roles = Roles::where('tipo', 3)->get();

        return response()->json([
            "roles" => $roles
        ]);
    }
    public function getCliente(Request $request){
        $cliente = Cliente::where('usuario_id',$request->id)->first();

        return response()->json([
            "cliente"=>$cliente
        ]);
    }
}
