<?php

namespace App\Http\Controllers\Api\entrada;

use Exception;
use App\Http\Controllers\Controller;
use App\Models\Adquisicion;
use App\Models\Cliente;
use App\Models\Compania;
use App\Models\EntradaDetalle;
use App\Models\Entradas;
use App\Models\Linea;
use App\Models\LineaConsumo;
use App\Models\pedidosDetalle;
use App\Models\Proyecto;
use App\Models\ProyectoUsuario;
use App\Models\Sitio;
use App\Models\Trazabilidad;
use App\Models\Vendedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\isNull;

class EntradaController extends Controller
{
    public function getEntradas(Request $request)
    {
        try {
            // $lineas = Linea::get();
            $entradas = Entradas::with('compania', 'proveedor')->select('*')->orderBy('numero_entrada', 'desc')->get();

            foreach ($entradas as $entrada) {
                $dtLote = EntradaDetalle::where('numero_entrada',$entrada->numero_entrada)
                ->where('loteClave','!=',null)
                ->get();
                $entrada['conLotes'] = count($dtLote);

                $dtPedido = EntradaDetalle::where('numero_entrada',$entrada->numero_entrada)
                ->where('pedido_id','!=',null)
                ->get();
                $entrada['asignados'] = count($dtPedido);

            }

            return response()->json([
                "data" => $entradas
            ]);
            //code...
        } catch (Exception $e) {
            //throw $th;
            return response()->json([
                "data" => [],
                "error" => $e->getMessage()
            ]);
        }
    }

    public function getDetallesEntrada(Request $request)
    {
        $detalles = EntradaDetalle::with('cliente', 'vendedor', 'compania')
            ->where('numero_entrada', $request->numero_entrada)
            ->get();

        return response()->json([
            "detalles" => $detalles
        ]);
    }

    public function crearEntrada(Request $request)
    {
        $segmento = '1001';
        $fecha = now();
        $mes = $fecha->month < 10 ? '0' . $fecha->month : $fecha->month;
        $dia = $fecha->day < 10 ? '0' . $fecha->day : $fecha->day;
        $fechaString = $fecha->year . '' . $mes . '' . $dia;

        $numeroP = Entradas::select('*')->where("numero_entrada", 'like', $fechaString . '%')
            ->orderBy('numero_entrada', 'desc')
            ->first();

        if (!is_null($numeroP)) {
            $n = substr($numeroP['numero_entrada'], -4);
            $n = intval($n) + 1;
            $segmento = $n;
        }


        try {
            DB::beginTransaction();

            $entrada = new Entradas();
            $entrada->numero_entrada = $fechaString . '_' . $segmento;
            $entrada->carrier_entrada = $request->carrier_entrada;
            $entrada->fecha_entrada = $fecha;
            $entrada->cantidad_entrada = $request->cantidad_entrada;
            $entrada->precio_x_unidad = $request->precio_x_unidad;
            $entrada->total_entrada = $request->total_entrada;
            $entrada->save();


            DB::commit();

            return response()->json([
                "success" => true,
                "segmento" => $fechaString . '_' . $segmento
            ]);
            //code...
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                "success" => false,
                "error" => $e->getMessage()
            ]);
            //throw $th;
        }
    }

    public function getClientes(Request $request)
    {
        $vendedor = Vendedor::where('usuario_id', $request->id)->first();
        $clientes = Cliente::select('*');
        if ($vendedor) {
            $clientes->where('cve_vendedor', 'Like', '%' . $vendedor->cve_vendedor . '%');
        }
        return response()->json([
            "clientes" => $clientes->get()
        ]);
    }

    public function getVendodoresClientes()
    {
        $vendedores = Vendedor::get();
        $clientes = Cliente::get();
        $companias = Compania::get();

        return response()->json([
            "vendedores" => $vendedores,
            "clientes" => $clientes,
            "companias" => $companias
        ]);
    }

    public function guardarEntradaDet(Request $request)
    {
        $entrada = Entradas::where('numero_entrada', $request->numero_entrada)->first();



        try {

            if ($request->linea != '' && $request->linea != null) {
                $validar = EntradaDetalle::where('linea', $request->linea)->first();
                if (!isset($request->id) && $validar != null) {
                    throw new Exception('Linea: ' . $request->linea . ' existente');
                }
            }
            $validarICC = EntradaDetalle::where('ICC', $request->ICC)->first();
            if ($validarICC) {
                throw new Exception('ICC: ' . $request->imei . ' existente');
            }
            if ($request->imei != null) {
                $validarIMEI = EntradaDetalle::where('imei', $request->imei)->first();
                if ($validarIMEI) {
                    throw new Exception('IMEI: ' . $request->imei . ' existente');
                }
            }



            DB::beginTransaction();
            if ($entrada) {

                $detalle = new EntradaDetalle();
                if ($request->id) {
                    $detalle = EntradaDetalle::where('id', $request->id)->first();
                }
                $detalle->numero_entrada = $request->numero_entrada;
                $detalle->linea = $request->linea;
                $detalle->ICC = $request->ICC;
                $detalle->fecha = $request->fecha;
                $detalle->imei = $request->imei;
                $detalle->id_trazabilidad = $request->id_trazabilidad;
                $detalle->monto_para_activacion = $request->monto_para_activacion;

                $detalle->save();

                $countEntradasD = EntradaDetalle::where('numero_entrada', $request->numero_entrada)->where('asig_lote', '0')->get();
                $countEntradas = EntradaDetalle::where('numero_entrada', $request->numero_entrada)->get();

                $entrada->cantidad_entrada = count($countEntradas);
                $entrada->piezas_disponibles = count($countEntradasD);
                $entrada->total_entrada = count($countEntradas);
                $entrada->save();

                $trazabilidad = Trazabilidad::where('id', $request->id_trazabilidad)->first();
                if ($trazabilidad) {
                    $countTrazabilidad = EntradaDetalle::where('numero_entrada', $request->numero_entrada)
                        ->where('id_trazabilidad', $request->id_trazabilidad)
                        ->get();

                    $trazabilidad->cantidadRegistro = count($countTrazabilidad);
                    $trazabilidad->save();
                }
            }

            DB::commit();

            return response()->json([
                "success" => true
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            //throw $th;
            $obj = [
                "numero_entrada" => $request->numero_entrada,
                "linea" => $request->linea,
                "ICC" => $request->ICC,
                "fecha" => $request->fecha,
                "imei" => $request->imei,
                "monto_para_activacion" => $request->monto_para_activacion,
                "error" => $e->getMessage(),
            ];
            return response()->json([
                "success" => false,
                "error" => $e->getMessage(),
                "linea" => $e->getLine(),
                "obj" => $obj
            ]);
        }
    }

    public function eliminarDetalleEntrada(Request $request)
    {
        $detalle = EntradaDetalle::where('id', $request->id)->delete();

        $countEntradas = EntradaDetalle::where('numero_entrada', $request->numero_entrada)->get();
        $countEntradasD = EntradaDetalle::where('numero_entrada', $request->numero_entrada)->where('asig_lote', '0')->get();

        $trazabilidad = Trazabilidad::where('id', $request->id_trazabilidad)->first();
        if ($trazabilidad) {
            $countTrazabilidad = EntradaDetalle::where('id_trazabilidad', $request->id_trazabilidad)->get();
            $trazabilidad->cantidadRegistro = count($countTrazabilidad);
            $trazabilidad->save();
        }

        $entrada = Entradas::where('numero_entrada', $request->numero_entrada)->first();
        $entrada->total_entrada = count($countEntradas);
        $entrada->piezas_disponibles = count($countEntradasD);
        $entrada->save();

        return response()->json([
            "success" => true
        ]);
    }
    public function eliminarEntrada(Request $request)
    {
        $entrada = Entradas::where('id', $request->id)->first();

        $detalles = EntradaDetalle::where('numero_entrada', $entrada->numero_entrada)->delete();

        $entrada->delete();

        return response()->json([
            "success" => true
        ]);
    }

    public function actualizarEntrada(Request $request)
    {
        try {
            $entrada = Entradas::where('id', $request->id)->first();
            $entrada->fecha_entrada = $request->fecha_entrada;
            $entrada->precio_x_unidad = $request->precio_x_unidad;
            $entrada->cantidad_entrada = $request->cantidad_entrada;
            $entrada->completa = $request->completa;
            $entrada->totalprecio = $request->totalprecio;

            if ($request->compania != null) {
                $entrada->carrier_entrada = $request->compania['id'];
            }

            if ($request->proveedor != null) {
                $entrada->porveedor_id = $request->proveedor['id'];
            }
            $entrada->save();

            return response()->json([
                "success" => true
            ]);
        } catch (Exception $e) {
            return response()->json([
                "success" => false,
                "error" => $e->getMessage()
            ]);
        }
    }

    public function getCompanias()
    {
        $companias = Compania::get();
        return response()->json([
            "companias" => $companias
        ]);
    }
    public function subirListaEntrada(Request $request)
    {
        $listaF = array();
        $listasuccess = array();
        $countSuccess = 0;
        $entrada = Entradas::where('numero_entrada', $request->numero_entrada)->first();

        try {
            $lista = json_decode($request->lineas);
            foreach ($lista as $item) {
                $allValid = true;
                if (isset($item->linea)) {
                    if ($item->linea != '' && $item->linea != null) {
                        $validar = EntradaDetalle::where('linea', $item->linea)->first();
                        if ($validar != null) {
                            $item->error = "Linea ya existente";
                            array_push($listaF, $item);
                            $allValid = false;
                        }
                    }
                }

                $validarICC = EntradaDetalle::where('ICC', $item->ICC)->first();
                if ($validarICC && $allValid) {
                    // throw new Exception('ICC: ' . $item['ICC'] . ' existente');
                    $item->error = "ICC ya existente";
                    array_push($listaF, $item);
                    $allValid = false;
                }
                // $validarIMEI = EntradaDetalle::where('imei', $item['imei'])->first();
                // if ($validarIMEI && $allValid) {
                //     // throw new Exception('IMEI: ' . $item['imei'] . ' existente');
                //     $item['error'] = "IMEI ya existente";
                //     array_push($listaF, $item);
                //     $allValid = false;

                // }

                if ($allValid) {
                    if ($entrada) {

                        $detalle = new EntradaDetalle();
                        $detalle->numero_entrada = $entrada->numero_entrada;
                        if (isset($item->linea)) {
                            $detalle->linea = $item->linea;
                        }
                        $detalle->ICC = $item->ICC;
                        $detalle->fecha = now();
                        // $detalle->imei = $item['imei'];
                        $detalle->id_trazabilidad = $request->id_trazabilidad;
                        $detalle->monto_para_activacion = $item->monto_para_activacion;

                        $detalle->save();

                        $countEntradasD = EntradaDetalle::where('numero_entrada', $request->numero_entrada)->where('asig_lote', '0')->get();
                        $countEntradas = EntradaDetalle::where('numero_entrada', $request->numero_entrada)->get();

                        $entrada->cantidad_entrada = count($countEntradas);
                        $entrada->piezas_disponibles = count($countEntradasD);
                        // if (doubleval($entrada->precio_x_unidad) > 0) {
                        $entrada->total_entrada = count($countEntradas);
                        // }
                        $entrada->save();

                        $trazabilidad = Trazabilidad::where('id', $request->id_trazabilidad)->first();
                        if ($trazabilidad) {
                            $countTrazabilidad = EntradaDetalle::where('numero_entrada', $request->numero_entrada)
                                ->where('id_trazabilidad', $request->id_trazabilidad)
                                ->get();

                            $trazabilidad->cantidadRegistro = count($countTrazabilidad);
                            $trazabilidad->save();
                        }
                    }
                    $countSuccess++;
                    array_push($listasuccess, $item);
                }
            }
            return response()->json([
                "success" => count($listaF) == 0,
                "listaE" => $listaF,
                "countSuccess" => $countSuccess,
                "listaSuccess" => $listasuccess
            ]);
        } catch (Exception $e) {
            return response()->json([
                "success" => true,
                "listaE" => $listaF,
                "error" => $e->getMessage()
            ]);
        }


        return response()->json([
            "listaF" => $listaF,
            "success" => count($listaF) == 0
        ]);
    }

    public function guardarDetalle($item, Entradas $entrada)
    {
        try {
            $validar = EntradaDetalle::where('linea', $item['linea'])->first();
            if ($validar != null) {
                throw new Exception('Linea existente');
            }
            DB::beginTransaction();

            if ($entrada) {
                $entrada->cantidad_entrada = $entrada->cantidad_entrada + 1;
                $entrada->piezas_disponibles = $entrada->cantidad_entrada;
                if (doubleval($entrada->precio_x_unidad) > 0) {
                    $entrada->total_entrada = intval($entrada->cantidad_entrada) * doubleval($entrada->precio_x_unidad);
                }
                $entrada->save();


                $detalle = new EntradaDetalle();
                $detalle->numero_entrada = $entrada->numero_entrada;
                $detalle->linea = $item['linea'];
                $detalle->ICC = $item['ICC'];
                $detalle->fecha = $item['fecha'];
                $detalle->imei = $item['imei'];
                $detalle->monto_para_activacion = $item['monto_para_activacion'];

                $detalle->save();
            }

            DB::commit();
            return [
                "success" => true
            ];
        } catch (Exception $e) {
            DB::rollBack();
            return [
                "success" => false,
                "error" => 'Linea ya existente',
                "msg" => $e->getMessage()
            ];
        }
    }

    public function validarTrazabilidades(Request $request)
    {
        $entrada = Entradas::where('numero_entrada', $request->numero_entrada)->first();

        $trazos = Trazabilidad::where('numero_entrada', $request->numero_entrada)->get();
        // $completo = false;
        $countCompletos = 0;
        foreach ($trazos as $item) {
            if ($item->cantidad == $item->cantidadRegistro) {
                $countCompletos++;
            }
        }

        if ($countCompletos == count($trazos)) {
            if ($entrada) {
                $entrada->completa = 1;
                $entrada->save();
            }
        }

        return response()->json([
            "success" => true
        ]);
    }
}
