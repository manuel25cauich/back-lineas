<?php

namespace App\Http\Controllers\Api\proveedores;

use Exception;
use App\Http\Controllers\Controller;
use App\Models\Adquisicion;
use App\Models\Cliente;
use App\Models\Compania;
use App\Models\Linea;
use App\Models\LineaConsumo;
use App\Models\Pedidos;
use App\Models\pedidosDetalle;
use App\Models\Proveedores;
use App\Models\Proyecto;
use App\Models\ProyectoUsuario;
use App\Models\Sitio;
use App\Models\Vendedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\isNull;
use function PHPUnit\Framework\returnSelf;

class ProvedoresController extends Controller
{
    public function getProveedores()
    {
        $proveedores = Proveedores::orderBy('id', 'desc')->get();

        return response()->json([
            "proveedores" => $proveedores
        ]);
    }

    public function guardarProveedor(Request $request)
    {
        try {
            DB::beginTransaction();
            $proveedor = new Proveedores();
            if ($request->id) {
                $proveedor = Proveedores::where('id', $request->id)->first();
            }
            $numeroC = Proveedores::select('*')
                ->orderBy('id', 'desc')
                ->first();

            $segmento = '10000';

            if (!is_null($numeroC)) {
                $n = $numeroC['clave_proveedor'];
                $n = intval($n) + 1;
                $segmento = $n;
            }

            if (is_null($request->id)) {
                $proveedor->clave_proveedor = $segmento;
            }
            $proveedor->nombre_proveedor = $request->nombre_proveedor;
            $proveedor->rfc_proveedor = $request->rfc_proveedor;
            $proveedor->cp_proveedor = $request->cp_proveedor;
            $proveedor->calleynumero = $request->calleynumero;
            $proveedor->colonia = $request->colonia['d_asenta'] ?? $request->colonia;
            $proveedor->municipio = $request->municipio['D_mnpio'] ?? $request->municipio;
            $proveedor->estado = $request->estado['d_estado'] ?? $request->estado;
            $proveedor->contacto_nombre = $request->contacto_nombre;
            $proveedor->contacto_telefono = $request->contacto_telefono;
            $proveedor->save();

            DB::commit();
            return response()->json([
                "success"=>true
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                "success"=>false,
                "error"=>$e->getMessage(),
                "line"=>$e->getLine(),
                "msg"=>"Error, intente nuevamente más tarde"
            ]);
        }
    }

    public function eliminarProveedor(Request $request){
        $proveedor = Proveedores::where('id',$request->id)->first();

        if($proveedor){
            $proveedor->delete();
        }

        return response()->json([
            "success"=>true
        ]);
    }
}
