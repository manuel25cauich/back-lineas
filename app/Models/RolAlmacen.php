<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RolAlmacen extends Model
{
    protected $table = "rolAlmacen";

    public function tipoR(){
        return $this->hasOne(TipoRoles::class,"id","tipo");
    }
}
