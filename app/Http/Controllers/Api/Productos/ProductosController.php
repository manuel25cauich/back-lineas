<?php

namespace App\Http\Controllers\Api\Productos;

use Exception;
use App\Http\Controllers\Controller;
use App\Models\Adquisicion;
use App\Models\Cliente;
use App\Models\Compania;
use App\Models\ImagenProducto;
use App\Models\Linea;
use App\Models\LineaConsumo;
use App\Models\Pedidos;
use App\Models\pedidosDetalle;
use App\Models\PerfilVenta;
use App\Models\Productos;
use App\Models\Proveedores;
use App\Models\Proyecto;
use App\Models\ProyectoUsuario;
use App\Models\Sitio;
use App\Models\Vendedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use function PHPUnit\Framework\isNull;
use function PHPUnit\Framework\returnSelf;

class ProductosController extends Controller
{
    public function getProductos()
    {
        $productos = Productos::with('imagenes', 'perfilVenta')->orderBy('id', 'desc')->get();

        return response()->json([
            "productos" => $productos
        ]);
    }
    public function eliminarProducto(Request $request)
    {
        $producto = Productos::where('id', $request->id)->first();

        if ($producto) {
            $producto->delete();
        }

        return response()->json([
            "success" => true
        ]);
    }

    public function guardarProductos(Request $request)
    {
        try {
            DB::beginTransaction();
            $msg = "Guardardo con éxito";
            $producto = new Productos();
            if ($request->id) {
                $producto = Productos::where('id', $request->id)->first();
                $msg = "Actualizado con éxito";
            }

            $producto->sku = $request->sku;
            $producto->clave_alterna = $request->clave_alterna;
            $producto->nombre = $request->nombre;
            $producto->alias = $request->alias;
            $producto->descripcion = $request->descripcion;
            $producto->existencias = $request->existencias;
            $producto->precio_compra = $request->precio_compra;
            $producto->tasa_impuesto = $request->tasa_impuesto;
            $producto->precio_venta_sin_impuestos = $request->precio_venta_sin_impuestos;
            $producto->precio_venta_con_impuestos = $request->precio_venta_con_impuestos;
            $producto->ubicacion = $request->ubicacion;
            $producto->detalles = $request->detalles;
            $producto->aplica_detalle_venta_por_pieza = $request->aplica_detalle_venta_por_pieza;
            $producto->perfil_venta_por_pieza = $request->perfil_venta != null ? $request->perfil_venta['id'] : 0;
            $producto->medidas = $request->medidas;
            $producto->essim = $request->essim;
            $producto->save();

            DB::commit();

            return response()->json([
                "success" => true,
                "msg" => $msg
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                "success" => false,
                "msg" => "Error al guardar intente nuevamente",
                "error" => $e->getMessage()
            ]);
            //throw $th;
        }
    }

    public function cargar_imagen(Request $request)
    {
        try {
            DB::beginTransaction();
            // $file = $request->file('principal');
            // $name = $file->getClientOriginalName();

            $producto = Productos::where('id', $request->id)->first();
            if ($producto) {
                $imagenesP = ImagenProducto::where('id_producto', $request->id)->where('is_principal', 1)->first();
                if ($imagenesP) {
                    $imagenesP->is_principal = 0;
                    $imagenesP->save();
                }

                //$ruta = Storage::disk('public')->putFileAs('producto', $file, "{$producto->clave_alterna}_{$count}");
                if ($request->hasFile('principal')) {
                    // $path = Storage::disk('public')->putFile('producto',$request->file('principal'));
                    $path = Storage::putFile('public/producto', $request->file('principal'));
                    // $path = $request->file('principal')->store('producto', 'public');
                    $nPath = explode("/", $path);
                    $crearImagen = new ImagenProducto();
                    $crearImagen->url = $nPath[1] . '/' . $nPath[2];
                    $crearImagen->is_principal = $request->is_principal;
                    $crearImagen->id_producto = $request->id;
                    $crearImagen->save();
                }
            }
            DB::commit();

            return response()->json([
                "success" => true,
                "msg" => "Imagen cargada con éxito"
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                "success" => false,
                "msg" => "Error intente nuevamente",
                "error" => $e->getMessage()
            ]);
        }
    }

    public function eliminarImagen(Request $request)
    {
        $imagen = ImagenProducto::where('id', $request->id)->first();
        if ($imagen) {
            $imagen->delete();
        }

        return response()->json([
            "success" => true
        ]);
    }
    public function getImagenes(Request $request)
    {
        $imagenes = ImagenProducto::where('id_producto', $request->id)->get();

        return response()->json([
            "imagenes" => $imagenes
        ]);
    }
    public function cambiarImagenPrincipal(Request $request)
    {
        $anterior = ImagenProducto::where('id_producto', $request->id_producto)->where('is_principal', 1)->first();
        if ($anterior && $request->is_principal == 1) {
            $anterior->is_principal = 0;
            $anterior->save();
        }
        $imagen = ImagenProducto::where('id', $request->id)->first();
        $imagen->is_principal = $request->is_principal;
        $imagen->save();

        return response()->json([
            "success" => true
        ]);
    }

    public function getPerfilesVentaP(Request $request)
    {
        $perfiles = PerfilVenta::get();

        return response()->json([
            "perfiles" => $perfiles
        ]);
    }
}
