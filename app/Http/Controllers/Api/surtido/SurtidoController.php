<?php

namespace App\Http\Controllers\Api\surtido;

use Exception;
use App\Http\Controllers\Controller;
use App\Models\Adquisicion;
use App\Models\Cliente;
use App\Models\Compania;
use App\Models\EntradaDetalle;
use App\Models\Entradas;
use App\Models\ImagenProducto;
use App\Models\Linea;
use App\Models\LineaConsumo;
use App\Models\Lotes;
use App\Models\Pedidos;
use App\Models\pedidosDetalle;
use App\Models\PerfilVenta;
use App\Models\Productos;
use App\Models\Proveedores;
use App\Models\Proyecto;
use App\Models\ProyectoUsuario;
use App\Models\Sitio;
use App\Models\Vendedor;
use AWS\CRT\HTTP\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use function PHPUnit\Framework\isNull;
use function PHPUnit\Framework\returnSelf;

class SurtidoController extends Controller
{
    public function getPedidosSurtidos(Request $request)
    {
        try {
            // $lineas = Linea::get();


            $pedidos = Pedidos::with('tipo', 'cliente', 'detalle.producto')->select('*')
                // ->where('tipoPedido', 0)
                ->where('eliminado', '0');

            if ($request->filtro == '1') {
                $pedidos->where(function ($query) {
                    $query->where('recibido_almacen', '0')
                        ->orWhere('surtiendo_almacen', '0')
                        ->orWhere('listo_almacen', '0')
                        ->orWhere('entrega_vendedor', '0');
                })->where('cancelado', '0');
            }




            if ($request->rol == 2) {
                $vendedor = Vendedor::where('usuario_id', $request->id)->first();
                if ($vendedor) {
                    $pedidos->where('clave_vendedor', $vendedor->cve_vendedor);
                }
            }


            return response()->json([
                "data" => $pedidos->orderBy('numero_pedido', 'desc')->get()
            ]);
            //code...
        } catch (Exception $e) {
            //throw $th;
            return response()->json([
                "data" => [],
                "error" => $e->getMessage()
            ]);
        }
    }
    public function getPedidoS(Request $request)
    {
        $pedido = Pedidos::with('detalle.producto', 'cliente')->where('id', $request->id)->first();


        return response()->json([
            "pedido" => $pedido
        ]);
    }

    public function getLoteSims(Request $request)
    {
        $datos = [];
        if ($request->ubicacion === '1') {
            $datos = Lotes::select('lote.numero_lote')
                ->join('entrada_det', 'entrada_det.loteClave', '=', 'lote.numero_lote')
                ->join('trazabilida_producto', 'entrada_det.id_trazabilidad', '=', 'trazabilida_producto.id')
                ->where('trazabilida_producto.producto_id', $request->producto['id'])
                ->where('pedido_id', null)
                ->groupBy('lote.numero_lote')
                ->get();

            foreach ($datos as $item) {
                $cantidad = EntradaDetalle::where('loteClave', $item->numero_lote)
                    ->where('pedido_id', null)
                    ->get();
                $item->cantidad = count($cantidad);
                $item->seleccionar = false;
            }
        } else {
            $datos = EntradaDetalle::select('entrada_det.*')
                ->join('trazabilida_producto', 'trazabilida_producto.id', 'entrada_det.id_trazabilidad')
                ->where('entrada_det.pedido_id', null)
                ->where('entrada_det.asig_lote', 0)
                ->where('trazabilida_producto.producto_id', $request->producto['id'])
                ->get();

            foreach ($datos as $item) {
                $item->seleccionar = false;
            }
        }

        return response()->json([
            "datos" => $datos
        ]);
    }

    public function agregarSimsPedidoLotes($request)
    {
        try {
            DB::beginTransaction();
            // $quitarPedido = EntradaDetalle::select('entrada_det.*')
            //     ->join('trazabilida_producto', 'entrada_det.id_trazabilidad', '=', 'trazabilida_producto.id')
            //     ->where('entrada_det.pedido_id',  $request['pedido_id'])
            //     ->where('trazabilida_producto.producto_id', $request['producto']['id'])
            //     ->get();

            // foreach ($quitarPedido as $item) {
            //     $item->pedido_id = null;
            //     $item->save();
            // }

            foreach ($request['lotesSeleccionables'] as  $lote) {
                $detalles = EntradaDetalle::where('loteClave', $lote['numero_lote'])
                    ->where('pedido_id', null)
                    ->get();

                foreach ($detalles as $item) {
                    $item->pedido_id = $request['pedido_id'];
                    $item->save();
                }

                $pedido = Pedidos::where('id', $request['pedido_id'])->first();
                if ($pedido) {
                    $pedidosDetalle = pedidosDetalle::where('sku', $request['producto']['sku'])
                        ->where('numero_pedido', $pedido->numero_pedido)
                        ->first();

                    if ($pedidosDetalle) {
                        $pedidosDetalle->asignados = $pedidosDetalle->asignados + count($detalles);
                        $pedidosDetalle->save();
                    }

                    $pedido->surtiendo_almacen = '1';
                    $pedido->fecha_surtido_almacen = now();
                    $pedido->save();
                }
            }

            DB::commit();
            return [
                "success" => true
            ];
        } catch (Exception $e) {
            DB::rollBack();
            return [
                "success" => false,
                "error" => $e->getMessage(),
                "linea" => $e->getLine()

            ];
        }
    }
    public function agregarSimsEntradas($request)
    {
        try {
            DB::beginTransaction();
            // $quitarPedido = EntradaDetalle::select('entrada_det.*')
            //     ->join('trazabilida_producto', 'entrada_det.id_trazabilidad', '=', 'trazabilida_producto.id')
            //     ->where('entrada_det.pedido_id',  $request['pedido_id'])
            //     ->where('trazabilida_producto.producto_id', $request['producto']['id'])
            //     ->get();

            // foreach ($quitarPedido as $item) {
            //     $item->pedido_id = null;
            //     $item->save();
            // }

            foreach ($request['detalleSelecionados'] as  $det) {
                $detalles = EntradaDetalle::where('id', $det['id'])
                    // ->where('pedido_id', null)
                    ->first();


                $detalles->pedido_id = $request['pedido_id'];
                $detalles->save();
            }

            $pedido = Pedidos::where('id', $request['pedido_id'])->first();
            if ($pedido) {
                $pedidosDetalle = pedidosDetalle::where('sku', $request['producto']['sku'])
                    ->where('numero_pedido', $pedido->numero_pedido)
                    ->first();

                if ($pedidosDetalle) {
                    $pedidosDetalle->asignados = $pedidosDetalle->asignados + count($request['detalleSelecionados']);
                    $pedidosDetalle->save();
                }

                $pedido->surtiendo_almacen = '1';
                $pedido->fecha_surtido_almacen = now();
                $pedido->save();
            }


            DB::commit();
            return [
                "success" => true
            ];
        } catch (Exception $e) {
            DB::rollBack();
            return [
                "success" => false,
                "error" => $e->getMessage(),
                "linea" => $e->getLine()
            ];
        }
    }

    public function pedidoListoAlmacen(Request $request)
    {
        $pedido = Pedidos::where('id', $request->id)->first();

        if ($pedido) {
            $pedido->listo_almacen = '1';
            $pedido->fecha_listo_almacen = now();
            $pedido->save();
        }

        return response()->json([
            "success" => true
        ]);
    }

    public function entregaVendedor(Request $request)
    {
        $pedido = Pedidos::where('id', $request->id)->first();

        if ($pedido) {
            $pedido->entrega_vendedor = '1';
            $pedido->fecha_entrega_vendedor = now();
            $pedido->save();
        }

        return response()->json([
            "success" => true
        ]);
    }

    public function entregaCliente(Request $request)
    {
        $pedido = Pedidos::where('id', $request->id)->first();

        if ($pedido) {
            $pedido->entrega_cliente = '1';
            $pedido->fecha_entrega_cliente = now();
            $pedido->save();
        }

        return response()->json([
            "success" => true
        ]);
    }

    public function guardardatosSeleccionados(Request $request)
    {
        //   pedido_id: this.pedido_id,
        //   producto: this.detalle.producto,
        //   lotesSeleccionables: this.lotesSeleccionables
        try {
            foreach ($request->detalle as $item) {
                if (isset($item['listaLotes'])) {
                    $query = [
                        "pedido_id" => $request->id,
                        "producto" => $item['producto'],
                        "lotesSeleccionables" => $item['listaLotes'],
                    ];
                    $msd = $this->agregarSimsPedidoLotes($query);
                    // return response()->json($msd);
                }
                if (isset($item['listasueltos'])) {
                    $query2 = [
                        "pedido_id" => $request->id,
                        "producto" => $item['producto'],
                        "detalleSelecionados" => $item['listasueltos'],
                    ];
                    $msd = $this->agregarSimsEntradas($query2);
                    // return response()->json($msd);
                }
            }

            return response()->json([
                "success" => true
            ]);
        } catch (Exception $e) {
            return response()->json([
                "success" => true,
                "msg" => $e->getMessage()
            ]);
        }
    }

    public function getLista(Request $request)
    {
        $loteXdetalle = EntradaDetalle::select("loteClave")
            ->where('pedido_id', $request->id)
            ->where('asig_lote', '1')
            // ->orderBy('loteClave', 'desc')
            ->groupBy('loteClave')
            ->get();

        $sueltos = EntradaDetalle::with('trazo.producto')
            ->where('entrada_det.pedido_id', $request->id)
            ->where('entrada_det.asig_lote', '0')
            ->get();



        $lotes = [];
        foreach ($loteXdetalle as $lote) {
            $detalleLote = EntradaDetalle::with('trazo.producto')
                ->where('loteClave', $lote->loteClave)
                ->get();
            $obj = [
                "lote" => $lote->loteClave,
                "detalles" => $detalleLote
            ];
            array_push($lotes, $obj);
        }

        $pedidos = pedidosDetalle::select('pedidos_det.*')
        ->where('pedidos_det.numero_pedido',$request->numero_pedido)
        ->join('productos','productos.sku','=','pedidos_det.sku')
        ->where('productos.aplica_detalle_venta_por_pieza','0')
        ->get();


        return response()->json([
            "loteXdetalle" => $loteXdetalle,
            "lotes" => $lotes,
            "sueltos" => $sueltos,
            "pedidos"=>$pedidos
        ]);
    }
}
