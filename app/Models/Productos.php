<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    protected $table = "productos";

    public function imagenes(){
        return $this->hasMany(ImagenProducto::class,"id_producto","id");

    }

    public function perfilVenta(){
        return $this->hasOne(PerfilVenta::class,"id","perfil_venta_por_pieza");

    }
}
