<?php

namespace App\Http\Controllers\Api\detalleEntrada;

use Exception;
use App\Http\Controllers\Controller;
use App\Models\Adquisicion;
use App\Models\Cliente;
use App\Models\Compania;
use App\Models\EntradaDetalle;
use App\Models\Entradas;
use App\Models\Linea;
use App\Models\LineaConsumo;
use App\Models\pedidosDetalle;
use App\Models\Productos;
use App\Models\Proyecto;
use App\Models\ProyectoUsuario;
use App\Models\Sitio;
use App\Models\Trazabilidad;
use App\Models\Vendedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\isNull;

class DetalleEntradaController extends Controller
{
    public function getDetallesTrazabilidad(Request $request)
    {
        $detalles = EntradaDetalle::with('cliente', 'vendedor', 'compania')->where('numero_entrada', $request->numero_entrada)
            ->where('id_trazabilidad', $request->id_trazabilidad)
            ->get();

        return response()->json([
            "detalles" => $detalles
        ]);
    }

    public function getTrazabilidad(Request $request)
    {
        $trazabilidad = Trazabilidad::with('producto')->where('id', $request->id)->first();

        return response()->json([
            "trazabilidad" => $trazabilidad
        ]);
    }

    public function guardarEntradaDetProducto(Request $request)
    {
        $entrada = Entradas::where('numero_entrada', $request->numero_entrada)->first();



        try {

            $validar = EntradaDetalle::where('numero_serie', $request->numero_serie)
                ->where('numero_entrada', $request->numero_entrada)
                ->where('id_trazabilidad', $request->id_trazabilidad)
                ->first();
            if (!isset($request->id) && $validar != null) {
                throw new Exception('Número de serie existente');
            }


            DB::beginTransaction();
            if ($entrada) {

                $detalle = new EntradaDetalle();
                if ($request->id) {
                    $detalle = EntradaDetalle::where('id', $request->id)->first();
                }
                $detalle->numero_entrada = $request->numero_entrada;
                $detalle->numero_serie = $request->numero_serie;
                // $detalle->linea = $request->linea;
                // $detalle->ICC = $request->ICC;
                // $detalle->fecha = $request->fecha;
                // $detalle->imei = $request->imei;
                $detalle->id_trazabilidad = $request->id_trazabilidad;
                // $detalle->monto_para_activacion = $request->monto_para_activacion;

                $detalle->save();

                $countEntradasD = EntradaDetalle::where('numero_entrada', $request->numero_entrada)->where('asig_lote', '0')->get();
                $countEntradas = EntradaDetalle::where('numero_entrada', $request->numero_entrada)->get();

                $entrada->cantidad_entrada = $entrada->cantidad_entrada + 1;
                $entrada->piezas_disponibles = count($countEntradasD);
                $entrada->total_entrada = count($countEntradas);
                $entrada->save();

                $trazabilidad = Trazabilidad::where('id', $request->id_trazabilidad)->first();
                if ($trazabilidad) {
                    $countTrazabilidad = EntradaDetalle::where('numero_entrada', $request->numero_entrada)
                        ->where('id_trazabilidad', $request->id_trazabilidad)
                        ->get();

                    $trazabilidad->cantidadRegistro = count($countTrazabilidad);
                    $trazabilidad->save();
                }
            }

            DB::commit();

            return response()->json([
                "success" => true
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            //throw $th;
            $obj = [
                "numero_entrada" => $request->numero_entrada,
                "linea" => $request->linea,
                "ICC" => $request->ICC,
                "fecha" => $request->fecha,
                "imei" => $request->imei,
                "monto_para_activacion" => $request->monto_para_activacion,
                "error" => "Linea ya existente"
            ];
            return response()->json([
                "success" => false,
                "error" => $e->getMessage(),
                "linea" => $e->getLine(),
                "obj" => $obj
            ]);
        }
    }

    public function subirListaProducto(Request $request)
    {
        $listaF = array();
        $listasuccess = array();
        $countSuccess = 0;
        $entrada = Entradas::where('numero_entrada', $request->numero_entrada)->first();
        try {
            foreach ($request->lista as $item) {
                $validar = EntradaDetalle::where('numero_serie', $item['numero_serie'])
                    ->where('numero_entrada', $request->numero_entrada)
                    ->where('id_trazabilidad', $request->id_trazabilidad)
                    ->first();
                if ($validar != null) {
                    $item['error'] = "Número de serie ya existente";
                    array_push($listaF, $item);
                }
                if (is_null($validar)) {
                    if ($entrada) {

                        $detalle = new EntradaDetalle();
                        $detalle->numero_entrada = $entrada->numero_entrada;
                        $detalle->numero_serie = $item['numero_serie'];
                        $detalle->id_trazabilidad = $request->id_trazabilidad;

                        $detalle->save();

                        $countEntradasD = EntradaDetalle::where('numero_entrada', $request->numero_entrada)->where('asig_lote', '0')->get();
                        $countEntradas = EntradaDetalle::where('numero_entrada', $request->numero_entrada)->get();

                        $entrada->cantidad_entrada = count($countEntradas);
                        $entrada->piezas_disponibles = count($countEntradasD);
                        // if (doubleval($entrada->precio_x_unidad) > 0) {
                        $entrada->total_entrada = count($countEntradas);
                        // }
                        $entrada->save();

                        $trazabilidad = Trazabilidad::where('id', $request->id_trazabilidad)->first();
                        if ($trazabilidad) {
                            $countTrazabilidad = EntradaDetalle::where('numero_entrada', $request->numero_entrada)
                                ->where('id_trazabilidad', $request->id_trazabilidad)
                                ->get();

                            $trazabilidad->cantidadRegistro = count($countTrazabilidad);
                            $trazabilidad->save();
                        }
                    }
                    $countSuccess++;
                    array_push($listasuccess, $item);
                }
            }
            return response()->json([
                "success" => count($listaF) == 0,
                "listaE" => $listaF,
                "countSuccess" => $countSuccess,
                "listaSuccess" => $listasuccess
            ]);
        } catch (Exception $e) {
            return response()->json([
                "success" => true,
                "listaE" => $listaF,
                "error" => $e->getMessage()
            ]);
        }


        return response()->json([
            "listaF" => $listaF,
            "success" => count($listaF) == 0
        ]);
    }
}
