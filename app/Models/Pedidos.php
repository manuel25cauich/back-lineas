<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pedidos extends Model
{
    protected $table = "pedidos";

    public function tipo(){
        return $this->hasOne(TipoPedido::class,"id","tipo_pedido_id");
    }
    public function cliente(){
        return $this->hasOne(Cliente::class,"cve_cliente","clave_cliente");

    }
    public function detalle(){
        return $this->hasMany(pedidosDetalle::class,"numero_pedido","numero_pedido");
        // return $this->hasMany(ArticuloXSucursal::class,"ID_ART","ART_ID");


    }

    public function vendedor(){
        return $this->hasOne(Vendedor::class,"cve_vendedor","clave_vendedor");
        // return $this->hasMany(ArticuloXSucursal::class,"ID_ART","ART_ID");


    }

}
