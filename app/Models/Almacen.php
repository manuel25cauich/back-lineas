<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Almacen extends Model
{
    protected $table = "almacen";

    public function roleAlmacen(){
        return $this->hasOne(RolAlmacen::class,"id","rol");
    }

    public function usuario(){
        return $this->hasOne(Usuario::class,"id","usuario_id");
    }
}
