<?php

namespace App\Http\Controllers\Api\Almacen;

use Exception;
use App\Http\Controllers\Controller;
use App\Models\Almacen;
use App\Models\Proyecto;
use App\Models\ProyectoUsuario;
use App\Models\RolAlmacen;
use App\Models\Roles;
use App\Models\TipoRoles;
use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AlmacenController extends Controller
{
    public function getAlmacenes(Request $request)
    {
        $usuario = Usuario::where('id',$request->id)->first();

        $almacenes = Almacen::with('roleAlmacen', 'usuario');
        if($usuario->rol != 1){
            $almacenes->where('usuario_id', $request->id);
        }

        return response()->json([
            "almacenes" => $almacenes->get()
        ]);
    }

    public function getRolesAlmacen()
    {
        $roles = RolAlmacen::get();

        return response()->json([
            "roles" => $roles
        ]);
    }

    public function createUpdateALmacen(Request $request)
    {
        try {
            DB::beginTransaction();
            $almacen = new Almacen();
            if ($request->id) {
                $almacen = Almacen::where("id", $request->id)
                    ->first();
            }

            if( $request->almacen == null){
                throw new Exception('Ingrese un nombre valido');
            }
            if( $request->role_almacen == null){
                throw new Exception('Debe seleccionar un rol');
            }
            $usuario = null;
            if($request->usuario_id == null){
                $usuario = $request->usuario['id'];
            }else{
                $usuario = $request->usuario_id;
            }

            $almacen->almacen = $request->almacen;

            $almacen->rol = $request->role_almacen['id'];
            $almacen->usuario_id = $usuario;
            $almacen->save();

            DB::commit();
            return response()->json([
                "success"=>true
            ]);
        } catch (Exception $e) {
            DB::rollback();
            //throw $th;
            return response()->json([
                "success"=>false,
                "error"=>$e->getMessage()
            ]);
        }
    }

    public function getUsuarios(){
        $usuarios = Usuario::where('rol','!=',1)->get();

        return response()->json([
            "usuarios"=>$usuarios
        ]);
    }
}
