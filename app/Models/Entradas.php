<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entradas extends Model
{
    protected $table = "entradas";

    public function compania(){
        return $this->hasOne(Compania::class,"id","carrier_entrada");
    }
    public function proveedor(){
        return $this->hasOne(Proveedores::class,"id","porveedor_id");
    }
}
