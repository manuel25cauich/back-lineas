<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EntradaDetalle extends Model
{
    protected $table = "entrada_det";
    public function vendedor()
    {
        return $this->hasOne(Vendedor::class, "id", "vendedor_id");
    }
    public function cliente()
    {
        return $this->hasOne(Cliente::class, "id", "cliente_id");
    }
    public function compania()
    {
        return $this->hasOne(Compania::class, "id", "compania");
    }

    public function trazo(){
        return $this->hasOne(Trazabilidad::class, 'id','id_trazabilidad');
    }

    public function pedido(){
        return $this->hasOne(Pedidos::class, 'id','pedido_id');
    }
}
