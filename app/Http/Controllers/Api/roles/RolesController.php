<?php

namespace App\Http\Controllers\Api\roles;

use Exception;
use App\Http\Controllers\Controller;
use App\Models\Proyecto;
use App\Models\ProyectoUsuario;
use App\Models\Roles;
use App\Models\TipoRoles;
use Illuminate\Http\Request;



class RolesController extends Controller
{
    public function getRoles()
    {
        $roles = Roles::with('tipoR')->get();

        return response()->json([
            "roles" => $roles
        ]);
    }
    public function getTipos()
    {
        $tipos = TipoRoles::get();

        return response()->json([
            "tipos" => $tipos
        ]);
    }
    public function guaradarRol(Request $request)
    {
        try {
            $rol = new Roles();

            if ($request->id) {
                $rol = Roles::where('id', $request->id)->first();
            }
            $rol->descripcion = $request->descripcion;
            $rol->tipo = $request->tipo_r['id'];
            $rol->save();

            return response()->json([
                "success" => true,
            ]);
        } catch (Exception $e) {
            return response()->json([
                "success" => false,
                "msg"=>"Error intente nuevamente",
                "error"=>$e->getMessage()
            ]);
        }
    }
}
