<?php

namespace App\Http\Controllers\Api\Adquisicion;

use Exception;
use App\Http\Controllers\Controller;
use App\Models\Adquisicion;
use App\Models\Cliente;
use App\Models\Compania;
use App\Models\Linea;
use App\Models\LineaConsumo;
use App\Models\Proyecto;
use App\Models\ProyectoUsuario;
use App\Models\Sitio;
use App\Models\Vendedor;
use Illuminate\Http\Request;

use function PHPUnit\Framework\isNull;

class AdquisicionController extends Controller
{
    public function getAdquisiciones(Request $request)
    {
        try {
            // $lineas = Linea::get();


            $adquisiciones = Adquisicion::select('*');



            return response()->json([
                "data" => $adquisiciones->get()
            ]);
            //code...
        } catch (Exception $e) {
            //throw $th;
            return response()->json([
                "data" => [],
                "error" => $e->getMessage()
            ]);
        }
    }

    public function getClientes(Request $request){
        $vendedor = Vendedor::where('usuario_id',$request->id)->first();
        $clientes = Cliente::select('*');
        if($vendedor){
            $clientes->where('cve_vendedor','Like','%'.$vendedor->cve_vendedor.'%');
        }
        return response()->json([
            "clientes"=>$clientes->get()
        ]);
    }


}
