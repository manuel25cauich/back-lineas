<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = "usuarios";

    public function roles(){
        return $this->hasOne(Roles::class,"id","rol");
    }

}
