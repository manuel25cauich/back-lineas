<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = "roles";

    public function tipoR(){
        return $this->hasOne(TipoRoles::class,"id","tipo");
    }
}
