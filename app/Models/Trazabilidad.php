<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trazabilidad extends Model
{
    protected $table = "trazabilida_producto";

    public function producto(){
        return $this->hasOne(Productos::class,"id","producto_id");
    }
}
