<?php

namespace App\Http\Controllers\Api\Login;

use Exception;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Usuario;



class LoginController extends Controller
{
    public function login(Request $request){

        try {
            $user = Usuario::with('roles')->where('correo', 'LIKE', $request->correo)
            ->where('password','LIKE',$request->password)
            ->first();

           return response()->json([
            "success"=>$user != null,
            "data"=>$user
           ]);
            //code...
        } catch (Exception $e) {
            //throw $th;
            return response()->json([
                "success"=>false,
                "data"=>$e->getMessage()
               ]);
        }
    }
    public function prueba(){
        return "prueba";
    }

}
