<?php

namespace App\Http\Controllers\Api\lotes;

use Exception;
use App\Http\Controllers\Controller;
use App\Models\Adquisicion;
use App\Models\Cliente;
use App\Models\Compania;
use App\Models\EntradaDetalle;
use App\Models\Entradas;
use App\Models\Linea;
use App\Models\LineaConsumo;
use App\Models\Lotes;
use App\Models\pedidosDetalle;
use App\Models\Productos;
use App\Models\Proyecto;
use App\Models\ProyectoUsuario;
use App\Models\Sitio;
use App\Models\Trazabilidad;
use App\Models\Vendedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\isNull;

class LotesController extends Controller
{
    public function getLotesEntradas(Request $request)
    {
        $entradas = Entradas::where('piezas_disponibles', '>', 0)
            ->where('carrier_entrada', $request->compania['id'])
            ->get();

        return response()->json([
            "entradas" => $entradas
        ]);
    }

    public function getLineasEntradas(Request $request)
    {
        $trazavilidad = Trazabilidad::where('producto_id', $request->compania['id'])->get();
        $lineas = [];

        foreach ($trazavilidad as $item) {
            $detalles = EntradaDetalle::where('id_trazabilidad', $item->id)
                ->where('asig_lote', '0')
                ->get();
            // array_push();
            foreach ($detalles as $obj) {
                array_push($lineas, $obj);
            }
        }

        // $lineas = EntradaDetalle::where('numero_entrada', $request->lote)
        // ->where('asig_lote', '0')
        // ->get();

        return response()->json([
            "lineas" => $lineas
        ]);
    }

    public function guardarLotes(Request $request)
    {
        try {
            $compani = "T";

            // switch ($request->compania['id']) {
            //     case 1:
            //         $compani = "T";
            //         break;
            //     case 2:
            //         $compani = "B";
            //         break;
            //     case 3:
            //         $compani = "A";
            //         break;
            //     case 4:
            //         $compani = "M";
            //         break;

            //     default:
            //         # code...
            //         break;
            // }

            DB::beginTransaction();
            $segmento = '1001';
            $fecha = now();
            $mes = $fecha->month < 10 ? '0' . $fecha->month : $fecha->month;
            $dia = $fecha->day < 10 ? '0' . $fecha->day : $fecha->day;
            $fechaString = $fecha->year . '' . $mes . '' . $dia;

            $numeroP = Lotes::select('*')->where("numero_lote", 'like', $fechaString .'_'.$compani. '%')
                ->orderBy('fecha', 'desc')
                ->first();

            if (!is_null($numeroP)) {
                $n = substr($numeroP['numero_lote'], -4);
                $n = intval($n) + 1;
                $segmento = $n;
            }

            $lote = new Lotes();
            $lote->numero_lote = $fechaString . '_' . $compani . $segmento;
            $lote->fecha = $fecha;
            $lote->carrier = $request->compania['id'];
            $lote->numero_entrada = $request->lote;
            $lote->save();

            $listasS = 0;
            foreach ($request->listaSims as $item) {
                $detalleEntrada = EntradaDetalle::where('id', $item['id'])->first();
                $detalleEntrada->fecha_asig_lote = $fecha;
                $detalleEntrada->asig_lote = '1';
                // $detalleEntrada->fecha_asig_lote = $fecha;
                $detalleEntrada->loteClave = $fechaString . '_' . $compani . $segmento;
                $detalleEntrada->save();

                $listasS++;

                $countListasAignacionLotes = EntradaDetalle::where('numero_entrada', $item['numero_entrada'])
                    ->where('asig_lote', '1')
                    ->get();

                $countEntradasD = EntradaDetalle::where('numero_entrada', $item['numero_entrada'])->where('asig_lote', '0')->get();

                $entrada = Entradas::where('numero_entrada', $item['numero_entrada'])->first();
                $entrada->piezas_disponibles = count($countEntradasD);
                $entrada->con_lote = count($countListasAignacionLotes);
                $entrada->con_lote_asignado = count($countListasAignacionLotes);
                //$entrada->total_entrada = $entrada->total_entrada + list;
                $entrada->save();
            }


            DB::commit();

            return response()->json([
                "success" => true,
                "segmento" => $fechaString . '_' . $segmento
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            //throw $th;
            return response()->json([
                "success" => false,
                "error" => $e->getMessage()
            ]);
        }
    }

    public function getProductosSims()
    {
        $productos = Productos::where('essim', '1')->get();

        return response()->json([
            "productos" => $productos
        ]);
    }
}
