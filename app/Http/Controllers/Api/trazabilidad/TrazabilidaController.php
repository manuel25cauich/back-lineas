<?php

namespace App\Http\Controllers\Api\trazabilidad;

use Exception;
use App\Http\Controllers\Controller;
use App\Models\Adquisicion;
use App\Models\Cliente;
use App\Models\Compania;
use App\Models\EntradaDetalle;
use App\Models\Entradas;
use App\Models\Linea;
use App\Models\LineaConsumo;
use App\Models\pedidosDetalle;
use App\Models\Proyecto;
use App\Models\ProyectoUsuario;
use App\Models\Sitio;
use App\Models\Trazabilidad;
use App\Models\Vendedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\isNull;

class TrazabilidaController extends Controller
{
    public function getTrazabilidad(Request $request)
    {
        $trazos = Trazabilidad::with('producto.perfilVenta')
        ->where('numero_entrada',$request->numero_entrada)
        ->orderBy('id', 'desc')->get();

        return response()->json([
            "trazos" => $trazos
        ]);
    }

    public function guardarTrazabilidad(Request $request)
    {
        try {

            $validarProducto = Trazabilidad::where('numero_entrada', $request->numero_entrada)
                ->where('producto_id', $request->producto['id'])->first();
            if ($validarProducto && is_null($request->id)) {
                throw new Exception('Producto repetido', 10);
            }

            DB::beginTransaction();
            $trazo = new Trazabilidad();
            if ($request->id) {
                $trazo = Trazabilidad::where('id', $request->id)->first();
            }

            $trazo->numero_entrada = $request->numero_entrada;
            $trazo->producto_id = $request->producto['id'];
            $trazo->cantidad = $request->cantidad;
            $trazo->precio_unitario = $request->precio_unitario;
            $trazo->total = $request->total;
            if($request->producto['aplica_detalle_venta_por_pieza'] == 0){
                $trazo->cantidadRegistro = $request->cantidad;
            }else{
                if($request->id){
                    $detalles = EntradaDetalle::where('id_trazabilidad',$request->id)->get();
                    $trazo->cantidadRegistro = count($detalles);
                }
            }
            $trazo->save();

            $entrada = Entradas::where('numero_entrada',$request->numero_entrada)->first();
            if($entrada){
                $trazos = Trazabilidad::where('numero_entrada',$request->numero_entrada)->get();
                $entrada->totalArticulos = count($trazos);
                $entrada->save();
            }

            DB::commit();
            return response()->json([
                "success" => true,
                "msg" => "Agregado con éxito"
            ]);
        } catch (Exception $e) {
            $msg = "Error, intente nuevamente";
            if ($e->getCode() == 10) {
                $msg = $e->getMessage();
            }
            DB::rollBack();
            return response()->json([
                "success" => false,
                "error" => $e->getMessage(),
                "msg" => $msg

            ]);
        }
    }
    public function eliminarTrazo(Request $request)
    {
        try {
            $trazo = Trazabilidad::where('id', $request->id)->first();
            $detalles = EntradaDetalle::where('id_trazabilidad', $request->id)->get();

            foreach ($detalles as $det) {
                $det->delete();
            }
            $trazo->delete();

            $entrada = Entradas::where('numero_entrada',$request->numero_entrada)->first();
            if($entrada){

                $countEntradasD = EntradaDetalle::where('numero_entrada',$request->numero_entrada)->where('asig_lote','0')->get();
                $countEntradas = EntradaDetalle::where('numero_entrada',$request->numero_entrada)->get();


                $trazos = Trazabilidad::where('numero_entrada',$request->numero_entrada)->get();
                $entrada->totalArticulos = count($trazos);
                $entrada->piezas_disponibles = count($countEntradasD);
                $entrada->total_entrada = count($countEntradas);
                $entrada->cantidad_entrada = count($countEntradas);
                $entrada->save();


            }

            return response()->json([
                "success"=>true,
                "msg"=>"Eliminado con éxito"
            ]);
        } catch (Exception $e) {
            return response()->json([
                "success"=>false,
                "msg"=>$e->getMessage()
            ]);
        }
    }
}
