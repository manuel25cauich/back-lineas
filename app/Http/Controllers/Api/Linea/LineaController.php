<?php

namespace App\Http\Controllers\Api\Linea;

use Exception;
use App\Http\Controllers\Controller;
use App\Models\Compania;
use App\Models\Linea;
use App\Models\LineaConsumo;
use App\Models\Proyecto;
use App\Models\ProyectoUsuario;
use App\Models\Sitio;
use Illuminate\Http\Request;

use function PHPUnit\Framework\isNull;

class LineaController extends Controller
{
    public function getLineas(Request $request)
    {
        try {
            // $lineas = Linea::get();
            $valid = false;

            $linea = Linea::select('lineas.*');



            return response()->json([
                "data" => $linea->get()
            ]);
            //code...
        } catch (Exception $e) {
            //throw $th;
            return response()->json([
                "data" => [],
                "error" => $e->getMessage()
            ]);
        }
    }

    public function registrarLinea(Request $request)
    {
        $listaError = [];
        $listaSuccess = [];

        try {
            foreach ($request->lineas as $item) {
                $validarLinea = Linea::where('linea','LIKE' ,'%'.$item['linea'].'%')->first();



                if ($validarLinea == null) {

                    $linea = new Linea();
                    $linea->linea = $item['linea'];
                    $linea->fecha = $item['fecha'];
                    $linea->user_id = $request->id;
                    $linea->save();


                    array_push($listaSuccess, $item);
                } else {
                    array_push($listaError, $item);
                }
            }
            $listaError= array_merge($listaError, $request->errores);

            return response()->json([
                "success" => true,
                "Lsuccess" => count($listaSuccess),
                "Lerror" => count($listaError),
                "dataE" => $listaError
            ]);
        } catch (Exception $e) {
            return response()->json([
                "success" => false,
                "Lsuccess" => count($listaSuccess),
                "Lerror" => count($listaError),
                "dataE" => $listaError,
                "msg"=>$e->getMessage()
            ]);
        }
    }
    public function getCompania(){
        $companias = Compania::get();

        return response()->json([
            "companias"=>$companias
        ]);
    }
}
