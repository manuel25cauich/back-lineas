<?php

use App\Http\Controllers\Api\Adquisicion\AdquisicionController;
use App\Http\Controllers\Api\Almacen\AlmacenController;
use App\Http\Controllers\Api\cliente\ClienteController;
use App\Http\Controllers\Api\Comisiones\ComisionesTelcelController;
use App\Http\Controllers\Api\detalleEntrada\DetalleEntradaController;
use App\Http\Controllers\Api\entrada\EntradaController;
use App\Http\Controllers\Api\Linea\LineaController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Login\LoginController;
use App\Http\Controllers\Api\lotes\LotesController;
use App\Http\Controllers\Api\monitoreored\MonitoreoRedController;
use App\Http\Controllers\Api\pedidoG\PedidoGeneralController;
// use App\Http\Controllers\Api\pedidos\PedidoGeneralController;
use App\Http\Controllers\Api\pedidos\PedidosController;
use App\Http\Controllers\Api\Productos\ProductosController;
use App\Http\Controllers\Api\proveedores\ProvedoresController;
use App\Http\Controllers\Api\proyectos\ProyectoController;
use App\Http\Controllers\Api\roles\RolesController;
use App\Http\Controllers\Api\surtido\SurtidoController;
use App\Http\Controllers\Api\trazabilidad\TrazabilidaController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('login', [LoginController::class, 'login']);


// // Linea
Route::post('getLinea', [LineaController::class, 'getLineas']);
Route::post('registrarLinea', [LineaController::class, 'registrarLinea']);
Route::post('getCompania', [LineaController::class, 'getCompania']);

//adquisiciones
Route::post('getAdquisiciones', [AdquisicionController::class, 'getAdquisiciones']);
Route::post('getClientes', [AdquisicionController::class, 'getClientes']);

//Pedidos
Route::post('getPedidos', [PedidosController::class, 'getPedidos']);
Route::post('guardarPedido', [PedidosController::class, 'guardarPedido']);
Route::post('eliminarPedido', [PedidosController::class, 'eliminarPedido']);
Route::post('getPedido', [PedidosController::class, 'getPedido']);
Route::post('guardarDetalles', [PedidosController::class, 'guardarDetalles']);
Route::post('cancelarpedido', [PedidosController::class, 'cancelarpedido']);
Route::post('confirmarPedido', [PedidosController::class, 'confirmarPedido']);
Route::post('editarPedido', [PedidosController::class, 'editarPedido']);



//clientes
Route::post('getClientesVendedor', [ClienteController::class, 'getClientesVendedor']);
Route::post('getVendedor', [ClienteController::class, 'getVendedor']);
Route::post('guardarCliente', [ClienteController::class, 'guardarCliente']);
Route::post('eliminarCliente', [ClienteController::class, 'eliminarCliente']);
Route::post('getVendedoresC', [ClienteController::class, 'getVendedoresC']);
Route::get('getRolesClientes', [ClienteController::class, 'getRolesClientes']);
Route::post('getCliente', [ClienteController::class, 'getCliente']);

//entradas
Route::post('getEntradas', [EntradaController::class, 'getEntradas']);
Route::post('getClientes', [EntradaController::class, 'getClientes']);
Route::post('crearEntrada', [EntradaController::class, 'crearEntrada']);
Route::post('getVendodoresClientes', [EntradaController::class, 'getVendodoresClientes']);
Route::post('guardarEntradaDet', [EntradaController::class, 'guardarEntradaDet']);
Route::post('getDetallesEntrada', [EntradaController::class, 'getDetallesEntrada']);
Route::post('eliminarDetalleEntrada', [EntradaController::class, 'eliminarDetalleEntrada']);
Route::post('eliminarEntrada', [EntradaController::class, 'eliminarEntrada']);
Route::post('actualizarEntrada', [EntradaController::class, 'actualizarEntrada']);
Route::post('getCompanias', [EntradaController::class, 'getCompanias']);
Route::post('subirListaEntrada', [EntradaController::class, 'subirListaEntrada']);
Route::post('validarTrazabilidades', [EntradaController::class, 'validarTrazabilidades']);

//preparacion de lotes
Route::post('getLotesEntradas', [LotesController::class, 'getLotesEntradas']);
Route::post('getLineasEntradas', [LotesController::class, 'getLineasEntradas']);
Route::post('guardarLotes', [LotesController::class, 'guardarLotes']);
Route::get('getProductosSims', [LotesController::class, 'getProductosSims']);

//provedores
Route::get('getProveedores', [ProvedoresController::class, 'getProveedores']);
Route::post('guardarProveedor', [ProvedoresController::class, 'guardarProveedor']);
Route::post('eliminarProveedor', [ProvedoresController::class, 'eliminarProveedor']);

//productos
Route::get('getProductos', [ProductosController::class, 'getProductos']);
Route::post('eliminarProducto', [ProductosController::class, 'eliminarProducto']);
Route::post('guardarProductos', [ProductosController::class, 'guardarProductos']);
Route::post('cargar-imagen', [ProductosController::class, 'cargar_imagen']);
Route::post('eliminarImagen', [ProductosController::class, 'eliminarImagen']);
Route::post('getImagenes', [ProductosController::class, 'getImagenes']);
Route::post('cambiarImagenPrincipal', [ProductosController::class, 'cambiarImagenPrincipal']);
Route::get('getPerfilesVentaP', [ProductosController::class, 'getPerfilesVentaP']);


//trazabilida_producto
Route::post('getTrazabilidad', [TrazabilidaController::class, 'getTrazabilidad']);
Route::post('guardarTrazabilidad', [TrazabilidaController::class, 'guardarTrazabilidad']);
Route::post('eliminarTrazo', [TrazabilidaController::class, 'eliminarTrazo']);

//detalles de la entrada
Route::post('getTrazabilidad-id', [DetalleEntradaController::class, 'getTrazabilidad']);
Route::post('getDetallesTrazabilidad', [DetalleEntradaController::class, 'getDetallesTrazabilidad']);
Route::post('guardarEntradaDetProducto', [DetalleEntradaController::class, 'guardarEntradaDetProducto']);
Route::post('subirListaProducto', [DetalleEntradaController::class, 'subirListaProducto']);


//pedido general
Route::post('getPedidosG', [PedidoGeneralController::class, 'getPedidosG']);
Route::post('guardarPedidoG', [PedidoGeneralController::class, 'guardarPedidoG']);
Route::post('eliminarPedidoG', [PedidoGeneralController::class, 'eliminarPedidoG']);
Route::post('getPedidoG', [PedidoGeneralController::class, 'getPedidoG']);
Route::post('guardarDetallesG', [PedidoGeneralController::class, 'guardarDetallesG']);
Route::post('cancelarpedidoG', [PedidoGeneralController::class, 'cancelarpedidoG']);
Route::post('confirmarPedidoG', [PedidoGeneralController::class, 'confirmarPedidoG']);
Route::post('editarPedidoG', [PedidoGeneralController::class, 'editarPedidoG']);

//surtidos
Route::post('getPedidosSurtidos', [SurtidoController::class, 'getPedidosSurtidos']);
Route::post('getPedidoS', [SurtidoController::class, 'getPedidoS']);
Route::post('getLoteSims', [SurtidoController::class, 'getLoteSims']);
// Route::post('agregarSimsPedidoLotes', [SurtidoController::class, 'agregarSimsPedidoLotes']);
// Route::post('agregarSimsEntradas', [SurtidoController::class, 'agregarSimsEntradas']);
Route::post('pedidoListoAlmacen', [SurtidoController::class, 'pedidoListoAlmacen']);
Route::post('entregaVendedor', [SurtidoController::class, 'entregaVendedor']);
Route::post('entregaCliente', [SurtidoController::class, 'entregaCliente']);
Route::post('guardardatosSeleccionados', [SurtidoController::class, 'guardardatosSeleccionados']);
Route::post('getLista', [SurtidoController::class, 'getLista']);


//roles
Route::get('getRoles', [RolesController::class, 'getRoles']);
Route::get('getTipos', [RolesController::class, 'getTipos']);
Route::post('guaradarRol', [RolesController::class, 'guaradarRol']);

//almacenes
Route::prefix("almacenes")->name("almacenes.")->group(function () {
    Route::post('', [AlmacenController::class, 'getAlmacenes']);
    Route::post('createUpdateALmacen', [AlmacenController::class, 'createUpdateALmacen']);
    Route::get('roles-almacen', [AlmacenController::class, 'getRolesAlmacen']);
    Route::get('getUsuarios', [AlmacenController::class, 'getUsuarios']);
});


//comisiones telcel
Route::prefix("comision-telcel")->name("comision-tel.")->group(function () {
    Route::post('', [ComisionesTelcelController::class, 'getComisionesTelcel']);
    Route::post('guardar-comision', [ComisionesTelcelController::class, 'guardarDatos']);
    Route::post('datos-comision', [ComisionesTelcelController::class, 'datosComisiones']);
});

// 