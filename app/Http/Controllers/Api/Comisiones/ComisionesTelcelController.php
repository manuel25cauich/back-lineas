<?php

namespace App\Http\Controllers\Api\Comisiones;

use Exception;
use App\Http\Controllers\Controller;
use App\Models\Adquisicion;
use App\Models\Cliente;
use App\Models\ComisionTelcel;
use App\Models\Compania;
use App\Models\EntradaDetalle;
use App\Models\Entradas;
use App\Models\Linea;
use App\Models\LineaConsumo;
use App\Models\Pedidos;
use App\Models\pedidosDetalle;
use App\Models\Productos;
use App\Models\Proyecto;
use App\Models\ProyectoUsuario;
use App\Models\Sitio;
use App\Models\Trazabilidad;
use App\Models\Vendedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\isNull;

class ComisionesTelcelController extends Controller
{
    public function getComisionesTelcel(Request $request)
    {
        $comision = ComisionTelcel::get();

        return response()->json([
            "comision" => $comision
        ]);
    }

    public function guardarDatos(Request $request)
    {

        try {
            DB::beginTransaction();
            $lista = json_decode($request->lineas);

            foreach ($lista as $item) {
                // return response()->json($item->fuerzaVenta);
                // $item = $request->lineas;

                $newComision = new ComisionTelcel();
                $newComision->fuerzaVenta = $item->fuerzaVenta ?? null;
                $newComision->cadenaComercial = $item->cadenaComercial ?? null;
                $newComision->subFuerza = $item->subFuerza ?? null;
                $newComision->numeroC = $item->numeroC ?? null;
                $newComision->producto = $item->producto ?? null;
                $newComision->ICCID = $item->ICCID ?? null;
                $newComision->estatusLineaActual = $item->estatusLineaActual ?? null;
                $newComision->fechaPrimerIngreso = $item->fechaPrimerIngreso ?? null;
                $newComision->fechaPrimerRecarga = $item->fechaPrimerRecarga ?? null;
                $newComision->mesPrimerIngreso = $item->mesPrimerIngreso ?? null;
                $newComision->cicloEvaluacion = $item->cicloEvaluacion ?? null;
                $newComision->estatusComision = $item->estatusComision ?? null;
                $newComision->motivoD = $item->motivoD ?? null;
                $newComision->comision = $item->comision ?? null;
                $newComision->usuario_id = $request->id;
                $newComision->save();
            }
            DB::commit();
            return response()->json([
                "success" => true
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                "success" => false,
                "error" => $e->getMessage()
            ]);
            //throw $th;
        }
    }

    public function datosComisiones(Request $request)
    {
        $comisiones = ComisionTelcel::select('ICCID','mesPrimerIngreso', DB::raw('count(*) as total'))
            ->groupBy('ICCID','mesPrimerIngreso')
            ->get()
            ->keyBy(function($item) {
                return $item['ICCID'] ;
            });

        $iccidList = $comisiones->keys();
        $comisionesRelacionadas = ComisionTelcel::whereIn('ICCID', $iccidList)->get()->groupBy('ICCID');

        foreach ($comisiones as $iccid => $item) {
            $item->comisiones = $comisionesRelacionadas->get($iccid, collect());
        }

        $detallesEntrada = EntradaDetalle::with('pedido.cliente','pedido.vendedor')->get();


        return response()->json([
            "comisiones" => $comisiones,
            "detallesEntrada"=>$detallesEntrada
        ]);
    }
}
